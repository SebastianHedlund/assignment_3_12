package scania.sebastian.task3_12.dataaccess;

import scania.sebastian.task3_12.models.domain.Guitar;
import scania.sebastian.task3_12.models.dto.GuitarBrandsDto;

import java.util.ArrayList;

public interface IGuitarRepository {
    ArrayList<Guitar> getAllGuitars();
    Guitar getGuitar(int id);
    Guitar createGuitar(Guitar guitar);
    void replaceGuitar(Guitar guitar);
    void modifyGuitar(Guitar guitar);
    void deleteGuitar(int id);
    boolean guitarExists(int id);
    boolean isValidGuitar(Guitar guitar);
    GuitarBrandsDto getTotalGuitarBrands();
}
