package scania.sebastian.task3_12.dataaccess;

import org.springframework.stereotype.Controller;
import scania.sebastian.task3_12.models.domain.Guitar;
import scania.sebastian.task3_12.models.dto.GuitarBrandsDto;
import scania.sebastian.task3_12.models.maps.GuitarDtoMap;

import java.util.ArrayList;
import java.util.HashMap;

@Controller
public class GuitarRepository implements IGuitarRepository{
    private HashMap<Integer, Guitar> guitars = seedGuitar();

    private HashMap<Integer, Guitar> seedGuitar(){
        HashMap<Integer, Guitar> guitars = new HashMap<>();
        guitars.put(1, new Guitar(1,"Fender", "Telecaster"));
        guitars.put(2, new Guitar(2,"Jackson", "Kelly"));

        return guitars;
    }

    @Override
    public ArrayList<Guitar> getAllGuitars() {
        return new ArrayList<>(guitars.values());
    }

    @Override
    public Guitar getGuitar(int id) {
        return guitars.get(id);
    }

    @Override
    public Guitar createGuitar(Guitar guitar) {
        guitars.put(guitar.getId(), guitar);
        return guitar;
    }

    @Override
    public void replaceGuitar(Guitar guitar) {
        if (guitarExists(guitar.getId()))
            guitars.remove(guitar.getId());

        createGuitar(guitar);
    }

    @Override
    public void modifyGuitar(Guitar guitar) {
        Guitar guitarToModify = guitars.get(guitar.getId());
        guitarToModify.setModel(guitar.getModel());
        guitarToModify.setBrand(guitar.getBrand());
    }

    @Override
    public void deleteGuitar(int id) {
        guitars.remove(id);
    }

    @Override
    public boolean guitarExists(int id) {
        return getGuitar(id) != null;
    }

    @Override
    public boolean isValidGuitar(Guitar guitar) {
        return guitar.getId() > 0 && guitar.getBrand() != null && guitar.getModel() != null;
    }

    public GuitarBrandsDto getTotalGuitarBrands(){
        return GuitarDtoMap.mapGuitarBrandsDto(guitars);
    }
}
