package scania.sebastian.task3_12.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@AllArgsConstructor
@Getter
@Setter
public class GuitarBrandsDto {
    private HashMap<String, Integer> guitarBrandCount;
}
