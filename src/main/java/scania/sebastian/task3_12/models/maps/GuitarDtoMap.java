package scania.sebastian.task3_12.models.maps;

import scania.sebastian.task3_12.models.domain.Guitar;
import scania.sebastian.task3_12.models.dto.GuitarBrandsDto;

import java.util.HashMap;

public class GuitarDtoMap {
    public static GuitarBrandsDto mapGuitarBrandsDto(HashMap<Integer, Guitar> guitars){
        HashMap<String, Integer> brandMap = new HashMap<>();
        for (Guitar guitar:guitars.values()) {
            String brand = guitar.getBrand();
            brandMap.put(brand, guitarBrandCounter(guitars, brand));
        }

        return new GuitarBrandsDto(brandMap);
    }

    private static int guitarBrandCounter(HashMap<Integer, Guitar> guitars, String brand){
        return (int) guitars
                .values()
                .stream()
                .filter(guitar -> guitar.getBrand().equals(brand))
                .count();

    }

}
