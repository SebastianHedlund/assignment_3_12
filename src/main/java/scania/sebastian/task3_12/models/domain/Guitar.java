package scania.sebastian.task3_12.models.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Guitar {
    private int id;
    private String brand;
    private String model;
}
