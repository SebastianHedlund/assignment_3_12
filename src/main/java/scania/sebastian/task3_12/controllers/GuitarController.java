package scania.sebastian.task3_12.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.task3_12.dataaccess.GuitarRepository;
import scania.sebastian.task3_12.models.domain.Guitar;

import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/guitars")
public class GuitarController {
    private GuitarRepository guitarRepository;

    @Autowired
    public GuitarController(GuitarRepository guitarRepository){
        this.guitarRepository = guitarRepository;
    }

    @GetMapping
    public ResponseEntity<List<Guitar>> getGuitar(){
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable int id){
        if(!guitarRepository.guitarExists(id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
    }

    @GetMapping("/brands")
    public ResponseEntity<HashMap<String, Integer>> getTotalGuitarBrands(){
        return new ResponseEntity<>(guitarRepository.getTotalGuitarBrands().getGuitarBrandCount(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Guitar> createGuitar(@RequestBody Guitar guitar){
        if (!guitarRepository.isValidGuitar(guitar))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        if(guitarRepository.guitarExists(guitar.getId()))
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);

        return new ResponseEntity<>(guitarRepository.createGuitar(guitar), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Guitar> replaceGuitar(@RequestBody Guitar guitar){
        if (!guitarRepository.isValidGuitar(guitar))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        guitarRepository.replaceGuitar(guitar);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @PatchMapping
    public ResponseEntity<Guitar> modifyGuitar(@RequestBody Guitar guitar){
        if (!guitarRepository.isValidGuitar(guitar))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        if(!guitarRepository.guitarExists(guitar.getId()))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        guitarRepository.modifyGuitar(guitar);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Guitar> deleteGuitar(@PathVariable int id){
        if(!guitarRepository.guitarExists(id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        guitarRepository.deleteGuitar(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }


}
